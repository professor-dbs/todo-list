<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateTodoListTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('todo_list', function (Blueprint $table) {
            $table->id();
            $table->enum('status', ['todo', 'done'])->default('todo');
            $table->tinyInteger('priority');
            $table->string('title');
            $table->text('description');
            $table->unsignedBigInteger('parent_id')->nullable();
            $table->unsignedBigInteger('user_id');
            $table->dateTime('completed_at')->nullable();
            $table->timestamps();
            $table->foreign('parent_id', 'todo_list_parent_id_fkey')->references('id')->on('todo_list')->onUpdate('CASCADE')->onDelete('CASCADE');
        });

        DB::statement('ALTER TABLE todo_list ADD FULLTEXT fulltext_index (title)');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('todo_list');
    }
}
