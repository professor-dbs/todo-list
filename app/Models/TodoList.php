<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class TodoList extends Model
{
    use HasFactory;

    public $table = 'todo_list';

    public $fillable = [
        'status',
        'priority',
        'title',
        'description',
        'parent_id',
        'user_id',
        'completed_at'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'status' => 'string',
        'priority' => 'integer',
        'title' => 'string',
        'description' => 'string',
        'parent_id' => 'integer',
        'user_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'status' => 'required|in:todo,done',
        'priority' => 'required|integer',
        'title' => 'string|max:255',
        'description' => 'string',
        'parent_id' => 'integer',
        'user_id' => 'integer',
        'completed_at' => 'nullable',
        'created_at' => 'nullable',
        'updated_at' => 'nullable',
        'deleted_at' => 'nullable',
    ];


    /**
     * @return HasMany
     */
    public function subTask(): HasMany
    {
        return $this->hasMany(TodoList::class, 'parent_id')->with('subTask');
    }

    /**
     * @return HasMany
     */
    public function openSubTask(): HasMany
    {
        return $this->subTask()->where('status', 'todo')->with('openSubTask');
    }
}
