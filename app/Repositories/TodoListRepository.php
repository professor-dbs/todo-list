<?php

namespace App\Repositories;

use App\Models\TodoList;

/**
 * Class ItemRepository
 * @package App\Repositories
 * @version January 18, 2021, 10:09 am UTC
*/

class TodoListRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'status',
        'priority',
        'title',
        'description',
        'parent_id',
        'user_id'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return TodoList::class;
    }
}
