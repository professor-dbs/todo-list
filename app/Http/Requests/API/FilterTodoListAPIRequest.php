<?php

namespace App\Http\Requests\API;

use App\Models\TodoList;

class FilterTodoListAPIRequest extends AppBaseAPIRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules['status'] = 'in:todo,done';
        $rules['priority_from'] = 'integer';
        $rules['priority_to'] = 'required_with:priority_from|integer';
        $rules['title'] = 'string';
        $rules['sort_by'] = 'string|in:created_at,completed_at,priority';
        $rules['sort_direction'] = 'required_with:sort_by|in:ASC,DESC';

        return $rules;
    }
}
