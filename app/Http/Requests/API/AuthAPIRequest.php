<?php

namespace App\Http\Requests\API;

use App\Models\User;

class AuthAPIRequest extends AppBaseAPIRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return User::$rules;
    }
}
