<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\AuthAPIRequest;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AuthAPIController extends AppBaseAPIController
{
    public function register(AuthAPIRequest $request)
    {
        $user = User::create([
           'name' => $request['name'],
           'email' => $request['email'],
           'password' => Hash::make($request['password']),
       ]);

        $token = $user->createToken('auth_token')->plainTextToken;

        return $this->sendResponse(['access_token' => $token], 'User registered successfully');
    }

    public function login(Request $request)
    {
        if (!Auth::attempt($request->only('email', 'password'))) {
            return $this->sendError('Invalid login details', Response::HTTP_UNAUTHORIZED);
        }

        $user = User::where('email', $request['email'])->firstOrFail();

        $token = $user->createToken('auth_token')->plainTextToken;

        return $this->sendResponse(['access_token' => $token], 'User logged in successfully');
    }
}
