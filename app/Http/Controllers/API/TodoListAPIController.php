<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\FilterTodoListAPIRequest;
use App\Http\Requests\API\TodoListAPIRequest;
use App\Http\Requests\API\TodoListMarkCompletedAPIRequest;
use App\Http\Resources\TodoListResource;
use App\Repositories\TodoListRepository;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Resources\Json\JsonResource;

class TodoListAPIController extends AppBaseAPIController
{
    /** @var  TodoListRepository */
    private $todoListRepository;

    public function __construct(TodoListRepository $todoListRepo)
    {
        $this->todoListRepository = $todoListRepo;
    }

    /**
     * @return array|JsonResponse|JsonResource
     */
    public function index(FilterTodoListAPIRequest $request)
    {
        $todoListBuilder = $this->todoListRepository->allQuery(
            [
                'parent_id' => null,
                'user_id' => $request->user()->getKey(),
            ]
        );

        if ($request->get('status')) {
            $todoListBuilder->where('status', $request->get('status'));
        }

        if ($request->get('priority_from')) {
            $todoListBuilder->whereBetween('priority', [$request->get('priority_from'), $request->get('priority_to')]);
        }

        if ($request->get('title')) {
            $todoListBuilder->whereRaw(
                "MATCH(title) AGAINST(?)",
                array($request->get('title'))
            );
        }

        if ($request->get('sort_by')) {
            $todoListBuilder->orderBy($request->get('sort_by'), $request->get('sort_direction'));
        }
            return $this->sendResponse(
            TodoListResource::collection($todoListBuilder->get()),
            'TODO List retrieved successfully'
        );
    }

    /**
     * @param int $id
     * @return JsonResponse|JsonResource
     */
    public function show($id)
    {
        $todoListItem = $this->todoListRepository->find($id);

        if (empty($todoListItem)) {
            return $this->sendError('TODO List item not found');
        }

        return $this->sendResponse(new TodoListResource($todoListItem), 'TODO List item retrieved successfully');
    }

    /**
     * @param TodoListAPIRequest $request
     * @return array|JsonResponse|JsonResource
     */
    public function store(TodoListAPIRequest $request)
    {
        $input = $request->all();
        $input['user_id'] = $request->user()->getKey();

        $todoListItem = $this->todoListRepository->create($input);

        return $this->sendResponse(new TodoListResource($todoListItem), 'TODO List item saved successfully');
    }

    /**
     * @param $id
     * @param TodoListAPIRequest $request
     * @return array|JsonResponse|JsonResource
     */
    public function update($id, TodoListAPIRequest $request)
    {
        $input = $request->all();

        $todoListItem = $this->todoListRepository->find($id);

        if (empty($todoListItem)) {
            return $this->sendError('TODO List item not found');
        }

        $todoListItem = $this->todoListRepository->update($input, $id);

        return $this->sendResponse(new TodoListResource($todoListItem), 'TODO List item updated successfully');
    }

    /**
     * @param $id
     * @return array|JsonResponse|JsonResource
     */
    public function markCompleted($id)
    {
        $input = [
            "status" => "done",
            "completed_at" => Carbon::now()
        ];

        $todoListItem = $this->todoListRepository->find($id);

        if (empty($todoListItem)) {
            return $this->sendError('TODO List item not found');
        }

        if ($todoListItem->openSubTask->isNotEmpty()) {
            return $this->sendError('TODO List item can\'t be mark completed if has open subtasks');
        }

        $todoListItem = $this->todoListRepository->update($input, $id);

        return $this->sendResponse(new TodoListResource($todoListItem), 'TODO List item updated successfully');
    }

    /**
     * @param $id
     * @return JsonResponse
     */
    public function destroy($id)
    {
        $todoListItem = $this->todoListRepository->find($id);

        if (empty($todoListItem)) {
            return $this->sendError('TODO List item not found');
        }

        if ($todoListItem->status == 'done') {
            return $this->sendError('Done TODO List item can\'t be deleted');
        }

        $todoListItem->delete();

        return $this->sendSuccess('TODO List item deleted successfully');
    }
}
