<?php

use App\Http\Controllers\API\TodoListAPIController;
use App\Http\Controllers\API\AuthAPIController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::post('/register', [AuthAPIController::class, 'register']);
Route::post('/login', [AuthAPIController::class, 'login'])->name('login');

Route::group(['middleware' => 'auth:sanctum'], function () {
    Route::get('/', [TodoListAPIController::class, 'index']);
    Route::get('/todo-list/{id}', [TodoListAPIController::class, 'show']);
    Route::post('/todo-list', [TodoListAPIController::class, 'store']);
    Route::put('/todo-list/{id}', [TodoListAPIController::class, 'update']);
    Route::put('/todo-list/{id}/markCompleted', [TodoListAPIController::class, 'markCompleted']);
    Route::delete('/todo-list/{id}', [TodoListAPIController::class, 'destroy']);
});
